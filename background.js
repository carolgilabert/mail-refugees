var donateToCharity = function (walletIdentifier, walletPassword) {
    const amount = '0.00005'; //BTC
    const hhugsAddress = '1GyxGdKQSCTA6ZVQxwzxtbH3x9W3eFy8mE';
    const xhr = new XMLHttpRequest();

    xhr.open(
        "GET", 
        `https://blockchain.info/merchant/${walletIdentifier}/payment?password=${walletPassword}&to=${hhugsAddress}&amount=${amount}`, 
        false
    );
    xhr.send();

    const result = xhr.responseText;
    console.log(result);
    alert(`You have now donated ${amount}BTC to HHUGS :) `);
}

chrome.storage.sync.get(['walletIdentifier', 'walletPassword'], function (values) {
    donateToCharity(values.walletIdentifier, values.walletPassword);
});
