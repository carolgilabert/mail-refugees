const saveButton = document.getElementById('saveButton');
const walletIdentifierField = document.getElementById('walletIdentifier');
const walletPasswordField = document.getElementById('walletPassword');

chrome.storage.sync.get(['walletIdentifier', 'walletPassword'], function(values) {
    if (values.walletIdentifier) { walletIdentifierField.value = values.walletIdentifier; }
    if (values.walletPassword) { walletPasswordField.value = values.walletPassword; }
});

saveButton.addEventListener('click', function() {
    chrome.storage.sync.set({
        walletIdentifier: walletIdentifierField.value,
        walletPassword: walletPasswordField.value
    });
});
